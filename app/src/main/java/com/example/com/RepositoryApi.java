package com.example.com;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface RepositoryApi {
    @GET
    Call<User[]> getUsers(@Url String url);
}
