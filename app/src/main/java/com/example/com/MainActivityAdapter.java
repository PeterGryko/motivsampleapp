package com.example.com;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;

public class MainActivityAdapter extends ArrayAdapter<User> {

    private Context context;
    private int resource;
    private List<User> data;

    public MainActivityAdapter(@NonNull Context context, @LayoutRes int resource, List<User> data) {

        super(context, resource, data);
        this.context = context;
        this.resource = resource;
        this.data = data;
    }

    private static class ViewHolder {
        private TextView textview_1_1;
        private ImageView imageview_1_0;
        public View root;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            viewHolder.root = LayoutInflater.from(context).inflate(resource, null);
            viewHolder.imageview_1_0 = (ImageView) viewHolder.root.findViewById(R.id.imageview_1_0);
            viewHolder.textview_1_1 = (TextView) viewHolder.root.findViewById(R.id.textview_1_1);
            viewHolder.root.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        Picasso.with(context)
                .load(data.get(position).getAvatar_Url())
                .into(viewHolder.imageview_1_0);
        viewHolder.textview_1_1.setText(data.get(position).getLogin());
        return viewHolder.root;
    }

    public User getElement(int position) {
        return data.get(position);
    }
}
