package com.example.com;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;

public class RecyclerViewActivity extends AppCompatActivity {

    private RecyclerView recyclerview_1_0;
    private RecyclerViewActivityAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclerviewactivity);

        recyclerview_1_0 = (RecyclerView) findViewById(R.id.recyclerview_1_0);

        final ArrayList<User> data = new ArrayList<>();
        recyclerview_1_0.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerViewActivityAdapter(this, data, R.layout.recyclerviewactivityadapter);
        recyclerview_1_0.setAdapter(adapter);

        adapter.setOnItemClickListener(
                new RecyclerViewActivityAdapter.OnItemClickListener() {

                    @Override
                    public void onItemClick(
                            int position, User user, Pair<View, String>... sharedViews) {

                        Intent intent =
                                new Intent(RecyclerViewActivity.this, DetailsActivity.class);
                        intent.putExtra("data", user.toJson());
                        if (Build.VERSION.SDK_INT >= 16) {

                            ActivityOptionsCompat options =
                                    ActivityOptionsCompat.makeSceneTransitionAnimation(
                                            RecyclerViewActivity.this, sharedViews);
                            startActivity(intent, options.toBundle());

                        } else {
                            startActivity(intent);
                        }
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    }
                });

        Repository.getInstance(this)
                .getUsers(
                        new Repository.OnResponseListener<User[]>() {

                            @Override
                            public void onSuccess(User[] response) {

                                data.clear();
                                data.addAll(Arrays.asList(response));
                                adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onError(Exception error) {

                                Toast.makeText(
                                                RecyclerViewActivity.this,
                                                error.toString(),
                                                Toast.LENGTH_SHORT)
                                        .show();
                            }
                        });
    }
}
