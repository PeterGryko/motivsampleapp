package com.example.com;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Repository {

    public interface OnResponseListener<T> {

        public void onSuccess(T response);

        public void onError(Exception error);
    }

    private RepositoryApi api;
    private static Repository instance;

    private Repository() {

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
        Retrofit retrofit =
                new Retrofit.Builder()
                        .baseUrl("http://www.baseurlwhichidontwant.com")
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

        api = retrofit.create(RepositoryApi.class);
    }

    public static Repository getInstance(Context context) {

        if (instance == null) instance = new Repository();
        return instance;
    }

    public void getUsers(final OnResponseListener<User[]> onResponseListener) {

        api.getUsers("https://api.github.com/users")
                .enqueue(
                        new Callback<User[]>() {

                            @Override
                            public void onResponse(
                                    Call<User[]> call, retrofit2.Response<User[]> response) {
                                onResponseListener.onSuccess(response.body());
                            }

                            @Override
                            public void onFailure(Call<User[]> call, Throwable t) {
                                onResponseListener.onError(new Exception(t.getMessage()));
                            }
                        });
    }
}
