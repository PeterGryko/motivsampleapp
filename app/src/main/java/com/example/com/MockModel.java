package com.example.com;

import com.google.gson.Gson;

public class MockModel {

    private static final Gson gson = new Gson();
    private String label = "test";
    private int image = R.drawable.ic_launcher;
    private String image_url =
            "https://pbs.twimg.com/profile_images/737359467742912512/t_pzvyZZ_400x400.jpg";

    public MockModel() {}

    public MockModel(String label, int image, String image_url) {
        this.label = label;
        this.image = image;
        this.image_url = image_url;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setImage_Url(String image_url) {
        this.image_url = image_url;
    }

    public String getLabel() {
        return this.label;
    };

    public int getImage() {
        return this.image;
    };

    public String getImage_Url() {
        return this.image_url;
    };

    public static MockModel fromJson(String json) {
        return gson.fromJson(json, MockModel.class);
    }

    public String toJson() {
        return gson.toJson(this, MockModel.class);
    }
}
