package com.example.com;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private ListView listview_1_0;
    private MainActivityAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainactivity);

        listview_1_0 = (ListView) findViewById(R.id.listview_1_0);

        final ArrayList<User> data = new ArrayList<>();
        adapter = new MainActivityAdapter(this, R.layout.mainactivityadapter, data);
        listview_1_0.setAdapter(adapter);

        listview_1_0.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(
                            AdapterView<?> parent, View view, int position, long id) {

                        Intent intent = new Intent(MainActivity.this, RecyclerViewActivity.class);
                        User user = adapter.getElement(position);
                        intent.putExtra("data", user.toJson());
                        startActivity(intent);
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    }
                });

        Repository.getInstance(this)
                .getUsers(
                        new Repository.OnResponseListener<User[]>() {

                            @Override
                            public void onSuccess(User[] response) {

                                data.clear();
                                data.addAll(Arrays.asList(response));
                                adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onError(Exception error) {

                                Toast.makeText(
                                                MainActivity.this,
                                                error.toString(),
                                                Toast.LENGTH_SHORT)
                                        .show();
                            }
                        });
    }
}
