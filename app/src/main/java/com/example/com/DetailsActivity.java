package com.example.com;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {

    private TextView textview_1_1;
    private ImageView imageview_1_0;
    private Button button_1_2;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailsactivity);

        if (getIntent().getExtras() != null) {
            String data = getIntent().getStringExtra("data");
            user = User.fromJson(data);
        }

        textview_1_1 = (TextView) findViewById(R.id.textview_1_1);
        imageview_1_0 = (ImageView) findViewById(R.id.imageview_1_0);
        button_1_2 = (Button) findViewById(R.id.button_1_2);

        button_1_2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(DetailsActivity.this, LastActivity.class);
                        intent.putExtra("data", user.toJson());
                        if (Build.VERSION.SDK_INT >= 16) {

                            Pair<View, String> pairTest2 =
                                    new Pair<View, String>(textview_1_1, "test2");
                            Pair<View, String> pairTest =
                                    new Pair<View, String>(imageview_1_0, "test");
                            ActivityOptionsCompat options =
                                    ActivityOptionsCompat.makeSceneTransitionAnimation(
                                            DetailsActivity.this, pairTest2, pairTest);
                            startActivity(intent, options.toBundle());

                        } else {
                            startActivity(intent);
                        }
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    }
                });

        Picasso.with(DetailsActivity.this).load(user.getAvatar_Url()).into(imageview_1_0);
        textview_1_1.setText(user.getLogin());
    }
}
