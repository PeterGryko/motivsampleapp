package com.example.com;

import com.google.gson.Gson;

public class User {

    private static final Gson gson = new Gson();
    private String following_url;
    private String events_url;
    private String organizations_url;
    private String url;
    private String gists_url;
    private String html_url;
    private String subscriptions_url;
    private String avatar_url;
    private String repos_url;
    private String received_events_url;
    private String gravatar_id;
    private String starred_url;
    private boolean site_admin;
    private String login;
    private String type;
    private int id;
    private String followers_url;

    public User() {}

    public User(
            String following_url,
            String events_url,
            String organizations_url,
            String url,
            String gists_url,
            String html_url,
            String subscriptions_url,
            String avatar_url,
            String repos_url,
            String received_events_url,
            String gravatar_id,
            String starred_url,
            boolean site_admin,
            String login,
            String type,
            int id,
            String followers_url) {
        this.following_url = following_url;
        this.events_url = events_url;
        this.organizations_url = organizations_url;
        this.url = url;
        this.gists_url = gists_url;
        this.html_url = html_url;
        this.subscriptions_url = subscriptions_url;
        this.avatar_url = avatar_url;
        this.repos_url = repos_url;
        this.received_events_url = received_events_url;
        this.gravatar_id = gravatar_id;
        this.starred_url = starred_url;
        this.site_admin = site_admin;
        this.login = login;
        this.type = type;
        this.id = id;
        this.followers_url = followers_url;
    }

    public void setFollowing_Url(String following_url) {
        this.following_url = following_url;
    }

    public void setEvents_Url(String events_url) {
        this.events_url = events_url;
    }

    public void setOrganizations_Url(String organizations_url) {
        this.organizations_url = organizations_url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setGists_Url(String gists_url) {
        this.gists_url = gists_url;
    }

    public void setHtml_Url(String html_url) {
        this.html_url = html_url;
    }

    public void setSubscriptions_Url(String subscriptions_url) {
        this.subscriptions_url = subscriptions_url;
    }

    public void setAvatar_Url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public void setRepos_Url(String repos_url) {
        this.repos_url = repos_url;
    }

    public void setReceived_Events_Url(String received_events_url) {
        this.received_events_url = received_events_url;
    }

    public void setGravatar_Id(String gravatar_id) {
        this.gravatar_id = gravatar_id;
    }

    public void setStarred_Url(String starred_url) {
        this.starred_url = starred_url;
    }

    public void setSite_Admin(boolean site_admin) {
        this.site_admin = site_admin;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFollowers_Url(String followers_url) {
        this.followers_url = followers_url;
    }

    public String getFollowing_Url() {
        return this.following_url;
    };

    public String getEvents_Url() {
        return this.events_url;
    };

    public String getOrganizations_Url() {
        return this.organizations_url;
    };

    public String getUrl() {
        return this.url;
    };

    public String getGists_Url() {
        return this.gists_url;
    };

    public String getHtml_Url() {
        return this.html_url;
    };

    public String getSubscriptions_Url() {
        return this.subscriptions_url;
    };

    public String getAvatar_Url() {
        return this.avatar_url;
    };

    public String getRepos_Url() {
        return this.repos_url;
    };

    public String getReceived_Events_Url() {
        return this.received_events_url;
    };

    public String getGravatar_Id() {
        return this.gravatar_id;
    };

    public String getStarred_Url() {
        return this.starred_url;
    };

    public boolean getSite_Admin() {
        return this.site_admin;
    };

    public String getLogin() {
        return this.login;
    };

    public String getType() {
        return this.type;
    };

    public int getId() {
        return this.id;
    };

    public String getFollowers_Url() {
        return this.followers_url;
    };

    public static User fromJson(String json) {
        return gson.fromJson(json, User.class);
    }

    public String toJson() {
        return gson.toJson(this, User.class);
    }
}
